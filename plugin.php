<?php
/*
Plugin Name:	WC Custom Products Sorting| Kalashnikof
Description:	Plugin sorts products by quantity * price
Author:			Nikita Kalashnikov
Author URI:		https://kalashnikof.ru/products/wc-custom-products-sorting/

This plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.
*/
 
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if( ! class_exists('WC_Kalashnikof_Custom_Sorting') ) :

	class WC_Kalashnikof_Custom_Sorting{
		private static $instance;

		public function __construct(){
			// Includes plugin funstions
			add_action('plugins_loaded', array($this, 'kalashnikof_plugins_load'));
		}
		
		public function kalashnikof_plugins_load(){
			if (!class_exists('WooCommerce')) {
				add_action('admin_notices', array($this, 'kalashnikof_missing_wc_notice'));
				return;
			}

			require_once 'includes/class-wc-sorts-products.php';
		}

		public function kalashnikof_missing_wc_notice(){
			echo '<div class="error"><p><strong>' . sprintf('Sorts Products requires WooCommerce to be installed and active. You can download %s here.', '<a href="https://woocommerce.com/" target="_blank">WooCommerce</a>') . '</strong></p></div>';
		}

		public static function getInstance(){
			if (self::$instance === null) {
				self::$instance = new self();
			}
			return self::$instance;
		}

	}

	WC_Kalashnikof_Custom_Sorting::getInstance();
endif;