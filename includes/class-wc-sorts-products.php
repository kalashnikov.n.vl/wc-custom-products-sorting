<?php
if (!defined('ABSPATH')) {
    exit;
}

class WC_Kalashnikof_Handler{
    public function __construct(){
        // The code for displaying WooCommerce Product Custom Fields
        add_action( 'woocommerce_product_options_general_product_data', array($this, 'kalashnikof_product_custom_fields') ); 

        // Following code Saves  WooCommerce Product Custom Fields
        add_action( 'save_post', array($this, 'kalashnikof_product_custom_fields_save'), 10,3 );

        // Adding to admin order list bulk dropdown a custom action 'custom_downloads'
        add_filter( 'bulk_actions-edit-product', array( $this, 'kalashnikof_bulk_actions_edit_product' ) );

        // Make the action from selected orders
        add_filter( 'handle_bulk_actions-edit-product', array( $this, 'kalashnikof_handle_bulk_action_edit_product' ), 10, 3 );

        // The results notice from bulk action on orders
        add_action( 'admin_notices', array( $this, 'kalashnikof_bulk_action_admin_notice' ) );

        // Custom orderby products index
        add_filter('woocommerce_get_catalog_ordering_args', array( $this, 'kalashnikof_orderby_product_index' ) );
        
        // Custom orderby name
        add_filter( 'woocommerce_catalog_orderby', array( $this, 'kalashnikof_orderby_set_custom_title'), 5 );

        // add the action 
        add_action( 'woocommerce_restore_order_stock', array( $this, 'kalashnikof_change_product_index' ), 10, 1 );
        add_action( 'woocommerce_reduce_order_stock', array( $this, 'kalashnikof_change_product_index' ), 10, 1 );
        add_action( 'woocommerce_updated_product_stock', array( $this, 'kalashnikof_update_product_index_stock_update') );
    }

 
    public function kalashnikof_orderby_set_custom_title( $options ){
        $options['menu_order'] = __( 'Рекомендуемые', 'woocommerce' );

        return $options;
    }

    /*
    * Custom orderby
    */
    public function kalashnikof_orderby_product_index($args) {
        if ( isset( $_GET['orderby'] ) && 'menu_order' === $_GET['orderby'] || !isset($_GET['orderby']) ) {
            $args['meta_key'] = '_kalashnikof_product_index';
		    $args['orderby'] = array( 'meta_value_num' => 'DESC' );
        }

        return $args;
    }

    /*
    * Custom product meta
    */
    public function kalashnikof_product_custom_fields(){

        echo '<div class="product_custom_field">';
            // Custom Product Text Field
            woocommerce_wp_text_input(
                array(
                    'id' => '_kalashnikof_product_index',
                    'placeholder' => __('Индекс продукта', 'woocommerce'),
                    'label' => __('Индекс продукта', 'woocommerce'),
                    'desc_tip' => 'true',
                    'custom_attributes' => array('readonly' => 'readonly'),
                )
            );
        echo '</div>';
    }

    public function kalashnikof_product_custom_fields_save($post_id, $post, $update){
    
        if ( 'product' !== $post->post_type ) {
            return;
        }

        $price = intval($_POST['_regular_price']);
        $quantity = intval($_POST['_stock']);

        if ( !empty($price) && !empty($quantity) ){
            $product_index = $price * $quantity;
    
            update_post_meta($post_id, '_kalashnikof_product_index', $product_index);
        }

    }

    /*
    * Bulk functions
    */
    public function kalashnikof_bulk_actions_edit_product( $bulk_actions ) {
        $bulk_actions['update_product_index'] = __( 'Обновить индекс продуктов', 'woocommerce' );
        return $bulk_actions;
    }

    public function kalashnikof_handle_bulk_action_edit_product( $redirect, $action, $post_ids ) {
        if ( $action !== 'update_product_index' )
            return $redirect;

        foreach ( $post_ids as $post_id ) {
            $price = intval(get_post_meta( $post_id, '_regular_price', true));
            $quantity = intval(get_post_meta( $post_id, '_stock', true ));

            $product_index = $price * $quantity;

            update_post_meta( $post_id, '_kalashnikof_product_index', $product_index );
        }

        $redirect = add_query_arg('update_product_index', count( $post_ids ), $redirect );

        return $redirect;
    }
    
    public function kalashnikof_bulk_action_admin_notice() {
        if( ! empty( $_REQUEST['update_product_index'] ) ) {

            printf( '<div id="message" class="updated notice is-dismissible"><p>' .
                _n( 'Индекс %s продуктов обновлен.',
                'Индекс %s продуктов обновлен.',
                intval( $_REQUEST['update_product_index'] )
            ) . '</p></div>', intval( $_REQUEST['update_product_index'] ) );
    
        }
    }

    /*
    * Update index by change stock
    */
    public function kalashnikof_change_product_index( $order ) {
        $items = $order->get_items();

        foreach( $items as $item ) {
            $product_id = $item['product_id'];
    
            $price = intval(get_post_meta( $product_id, '_regular_price', true));
            $quantity = intval(get_post_meta( $product_id, '_stock', true ));

            $product_index = $price * $quantity;

            update_post_meta( $product_id, '_kalashnikof_product_index', $product_index );
        }
    }

    public function kalashnikof_update_product_index_stock_update( ) {

        $post_ids = get_posts( 
            array(
                'post_type' => 'product',
                'numberposts' => -1,
                'post_status' => 'publish',
                'fields' => 'ids',
            ) 
        );

        if ( !empty($post_ids) ){
            foreach ( $post_ids as $post_id ) {
                $price = intval(get_post_meta( $post_id, '_regular_price', true));
                $quantity = intval(get_post_meta( $post_id, '_stock', true ));
    
                $product_index = $price * $quantity;
    
                update_post_meta( $post_id, '_kalashnikof_product_index', $product_index );
            }
        }
    }

    /*
    * Log functions
    */
    public function log($data, $prefix = ''){
        if ($this->logging) {
            wc_get_logger()->debug("$prefix " . print_r($data, 1), ['source' => $this->id]);
        }
    }
}
new WC_Kalashnikof_Handler;